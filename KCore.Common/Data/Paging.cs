using System.Runtime.InteropServices.WindowsRuntime;
using Newtonsoft.Json;

namespace KCore.Common.Data
{
    public class Paging
    {
        public Paging()
        {
            Start = 0;
            Limit = 10;
        }

        [JsonProperty("s_")]
        public int Start { get; set; }
        [JsonProperty("l_")]
        public int Limit { get; set; }

        public static Paging Default =>  new Paging();
    }
}