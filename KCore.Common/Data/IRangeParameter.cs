namespace KCore.Common.Data
{
    public interface IRangeParameter : IParameter
    {
        object GetValue2();
        string TableName2 { get; set; }
        string ColumnName2 { get; set; }
        bool HasValue2 { get;  }
    }
}