namespace KCore.Common.Data
{
    public enum SqlOperator
    {
        Equals = 0,
        NotEqual = 1,
        GreatThan = 2,
        GreatThanEqual = 3,
        LessThan = 4,
        LesThanEqual = 5,
        BeginWith = 6,
        EndWith = 7,
        Like = 8,
        Beetween = 9,
        IsNull = 10
    }
}