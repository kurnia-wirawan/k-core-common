using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KCore.Common.Data
{
    public class WhereTerm : IParameter
    {
        
		public WhereTerm()
		{
			Operator = SqlOperator.Equals;
			ParamDataType = EnumParameterDataTypes.Character;
            Logical = LogicalOperator.AND;
		}

        /// <summary>
		/// Get value of whereterm
		/// </summary>
		/// <returns>object</returns>
		public object GetValue()
		{
			return Value;
		}

		/// <summary>
		/// Get & set name of table. 
		/// Notes: Must set this property if query using more than 1 table
		/// </summary>
		public string TableName { get; set; }

		/// <summary>
		/// Get & set column name. Must set this property for where term sql 
		/// </summary>
		public string ColumnName { get; set; }

		/// <summary>
		/// Get & Set Data type of column
		/// </summary>
		public EnumParameterDataTypes ParamDataType { get; set; }

		/// <summary>
		/// Get & Set Sql operator 
		/// </summary>
		public SqlOperator Operator { get; set; }

        public LogicalOperator Logical { get; set; }

		public bool HasValue => Value != null && !string.IsNullOrEmpty( Value.ToString());

		/// <summary>
		/// Get & set value of where term
		/// </summary>
		public object Value { get; set; }

        public static WhereTerm AllActiveRow =>
	        new WhereTerm
	        {
		        Value = DefaultValue.ROW_STATUS_ACTIVE,
		        TableName = string.Empty,
		        ParamDataType = EnumParameterDataTypes.Number,
		        Operator = SqlOperator.Equals,
		        ColumnName = DefaultValue.COLUMN_ROW_STATUS
	        };

        public static WhereTerm AllRow =>
	        new WhereTerm
	        {
		        Value = -1,
		        TableName = string.Empty,
		        ParamDataType = EnumParameterDataTypes.Number,
		        Operator = SqlOperator.GreatThanEqual,
		        ColumnName = DefaultValue.COLUMN_ROW_STATUS
	        };

        /// <summary>
		/// To create where term condition with default value TableName = string.Empty, ParamDataType = EnumParameterDataTypes.Character & Operator = SqlOperator.Equals
		/// </summary>
		/// <param name="value">Fill string value of where term</param>
		/// <param name="column">Fill string value of column name</param>
		/// <returns>WhereTerm</returns>
		public static WhereTerm Default(string value, string column)
		{
			return new WhereTerm
			{
				Value = value,
				TableName = string.Empty,
				ParamDataType = EnumParameterDataTypes.Character,
				Operator = SqlOperator.Equals,
				ColumnName = column
			};
		}
        public static WhereTerm Default(string value, string column, string tableName)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = tableName,
                ParamDataType = EnumParameterDataTypes.Character,
                Operator = SqlOperator.Equals,
                ColumnName = column
            };
        }
        /// <summary>
        /// To create where term condition with default value TableName = string.Empty, ParamDataType = EnumParameterDataTypes.Character
        /// </summary>
        /// <param name="value">Fill string value of where term</param>
        /// <param name="column">Fill string value of column name</param>
        /// <param name="sqlOperator">Fill sql operator</param>
        /// <returns>WhereTerm</returns>
        public static WhereTerm Default(string value, string column, SqlOperator sqlOperator)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.Character,
                Operator = sqlOperator,
                ColumnName = column
            };
        }

        public static WhereTerm Default(string value, string column, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.Character,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }

        public static WhereTerm Default(string value, string column, SqlOperator sqlOperator, LogicalOperator logical, EnumParameterDataTypes type)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = type,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }


        public static WhereTerm Default(Guid value, string column)
        {
            return new WhereTerm
            {
                Value = value.ToString(),
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.Guid,
                Operator = SqlOperator.Equals,
                ColumnName = column
            };
        }

        public static WhereTerm Default(Guid value, string column, SqlOperator sqlOperator)
        {
            return new WhereTerm
            {
                Value = value.ToString(),
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.Guid,
                Operator = sqlOperator,
                ColumnName = column
            };
        }


		/// <summary>
		/// To create where term condition with default value TableName = string.Empty, ParamDataType = EnumParameterDataTypes.DateTime
		/// </summary>
		/// <param name="value">Fill DateTime value of where term</param>
		/// <param name="column">Fill string value of column name</param>
		/// <param name="sqlOperator">Fill sql operator</param>
		/// <returns>WhereTerm</returns>
		public static WhereTerm Default(DateTime value, string column, SqlOperator sqlOperator)
		{
			return new WhereTerm
			{
				Value = value,
				TableName = string.Empty,
				ParamDataType = EnumParameterDataTypes.DateTime,
				Operator = sqlOperator,
				ColumnName = column
			};
		}
        public static WhereTerm Default(DateTime value, string column, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.DateTime,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }
        /// <summary>
        /// To create where term condition with default value TableName = string.Empty, ParamDataType = EnumParameterDataTypes.DateTime & Operator = SqlOperator.Equals
        /// </summary>
        /// <param name="value">Fill DateTime value of where term</param>
        /// <param name="column">Fill string value of column name</param>
        /// <returns>WhereTerm</returns>
        public static WhereTerm Default(DateTime value, string column)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.DateTime,
                Operator = SqlOperator.Equals,
                ColumnName = column
            };
        }
        public static WhereTerm Default(DateTime? value, string column)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.NullableDateTime,
                Operator = SqlOperator.Equals,
                ColumnName = column
            };
        }

		/// <summary>
		/// To create where term condition with default value TableName = string.Empty, ParamDataType = EnumParameterDataTypes.DateTime
		/// </summary>
		/// <param name="value">Fill integer value of where term</param>
		/// <param name="column">Fill string value of column name</param>
		/// <param name="sqlOperator">Fill sql operator</param>
		/// <returns>WhereTerm</returns>
		public static WhereTerm Default(int value, string column, SqlOperator sqlOperator)
		{
			return new WhereTerm
			{
				Value = value,
				TableName = string.Empty,
				ParamDataType = EnumParameterDataTypes.Number,
				Operator = sqlOperator,
				ColumnName = column
			};
		}

        public static WhereTerm Default(double value, string column, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.Number,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }

		

		/// <summary>
		/// To create where term condition with default value TableName = string.Empty, ParamDataType = EnumParameterDataTypes.Bool & Operator = SqlOperator.Equals
		/// </summary>
		/// <param name="value">Fill bool value of where term</param>
		/// <param name="column">Fill string value of column name</param>
		/// <returns>WhereTerm</returns>
		public static WhereTerm Default(bool value, string column)
		{
			return new WhereTerm
			{
				Value = value,
				TableName = string.Empty,
				ParamDataType = EnumParameterDataTypes.Bool,
				Operator = SqlOperator.Equals,
				ColumnName = column
			};
        }

        public static WhereTerm Default(bool value, string column, SqlOperator sqlOperator)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.Bool,
                Operator = sqlOperator,
                ColumnName = column
            };
        }

        /// <summary>
        /// To create where term condition with default value TableName = string.Empty, ParamDataType = EnumParameterDataTypes.Number & Operator = SqlOperator.Equals
        /// </summary>
        /// <param name="value">Fill int value of where term</param>
        /// <param name="column">Fill string value of column name</param>
        /// <returns>WhereTerm</returns>
        public static WhereTerm Default(int value, string column)
		{
			return new WhereTerm
			{
				Value = value,
				TableName = string.Empty,
				ParamDataType = EnumParameterDataTypes.Number,
				Operator = SqlOperator.Equals,
				ColumnName = column
			};
		}

        public static WhereTerm Default(object value, string column, string table)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = table,
                ParamDataType = EnumParameterDataTypes.Number,
                Operator = SqlOperator.Equals,
                ColumnName = column
            };
        }

        public static IParameter[] GetParameters(IParameter[] parameter, params IParameter[] parameters)
        {
            if (parameter == null)
            {
                return parameters;
            }
            var a = parameter.ToList();
            a.AddRange(parameters);
            return a.ToArray();
        }


		public static bool ResizeParameter(ref IParameter[] parameters, int size)
		{
			var hasRowParams = false;
			if (parameters == null)
			{
				Array.Resize(ref parameters, size);
			}
			else
			{
				if (parameters.Any(param => param.ColumnName.Equals(DefaultValue.COLUMN_ROW_STATUS)))
					hasRowParams = true;
				if (!hasRowParams)
					Array.Resize(ref parameters, parameters.Length + size);
			}
			return hasRowParams;
		}

        public static bool HasColumnParameter(ref IParameter[] parameters, string columnName, int size)
        {
            var hasRowParams = false;
            if (parameters == null)
            {
                Array.Resize(ref parameters, size);
            }
            else
            {
                if (parameters.Any(param => param.ColumnName.Equals(columnName)))
                    hasRowParams = true;
                if (!hasRowParams)
                    Array.Resize(ref parameters, parameters.Length + size);
            }
            return hasRowParams;
        }

        /// <summary>
        /// Jika parameters == null akan di tambahan WhereTerm RowStatus >= 0
        /// </summary>
        /// <param name="parameters"></param>
        public static void AddDefaultParameter(ref IParameter[] parameters)
        {
            if (parameters == null || parameters.Length == 0)
            {
                Array.Resize(ref parameters, 1);
                parameters[0] = Default(0, "RowStatus", SqlOperator.Equals) as IParameter;
            }
            else
            {
                var hasRowstatus = false;
                foreach (var listParameter in parameters.Where(listParameter => listParameter.ColumnName == "RowStatus"))
                {
                    hasRowstatus = true;
                }
                if (hasRowstatus) return;
                Array.Resize(ref parameters, parameters.Length + 1);
                parameters[parameters.Length -1] = Default(0, "RowStatus", SqlOperator.Equals) as IParameter;
            }
        }

        public static WhereTerm Default(int value, string column, SqlOperator sqlOperator, string tableName)
	    {
            return new WhereTerm
            {
                Value = value,
                TableName = tableName,
                ParamDataType = EnumParameterDataTypes.Number,
                Operator = sqlOperator,
                ColumnName = column
            };
	    }

        public static string GetParamaterForMessage(params IParameter[] parameters)
        {
            var sb = new StringBuilder();
            foreach (var listParameter in parameters)
            {
                sb.AppendLine($"<br/>Column: {listParameter.ColumnName} Value: {listParameter.GetValue()} ");
            }
            return sb.ToString();
        }

      


		public static List<IParameter> AddRowStatusTerm(List<IParameter> listParams, WhereTerm @default)
		{
			var hasRowStatus = false;
			// ReSharper disable UnusedVariable
			foreach (var listParameter in listParams.Where(listParameter => listParameter.ColumnName.Equals(DefaultValue.COLUMN_ROW_STATUS)))
			// ReSharper restore UnusedVariable
			{
				hasRowStatus = true;
			}
			if (!hasRowStatus)
				listParams.Add(Default(0, "RowStatus") as IParameter);
			return listParams;
		}



        public static WhereTerm Default(double? value, string column, SqlOperator sqlOperator, LogicalOperator logical)
	    {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.Number,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
	    }

        public static WhereTerm Default(DateTime? value, string column, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.DateTime,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }

        public static WhereTerm Default(Guid value, string column, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.Guid,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }

        public static WhereTerm Id(object value)
        {
            return new WhereTerm
            {
                Value = value,
                ParamDataType = EnumParameterDataTypes.Guid,
                Operator = SqlOperator.Equals,
                ColumnName = "Id"
            };
        }

	    public static WhereTerm Key(string applicationIDKey)
	    {
            return new WhereTerm
            {
                Value = applicationIDKey,
                ParamDataType = EnumParameterDataTypes.Character,
                Operator = SqlOperator.Equals,
                ColumnName = "ConfigKey"
            };
	    }

        public static WhereTerm Default(bool value, string column, SqlOperator sqlOperator, LogicalOperator logical)
	    {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParameterDataTypes.Bool,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
	    }

        public static IEnumerable<IParameter> DefaultListParameters => new List<IParameter>
        {
	        AllActiveRow
        };

	    
    }
}