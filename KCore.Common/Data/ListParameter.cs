using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace KCore.Common.Data
{
    public class ListParameter : IList<IParameter>
    {
        private readonly IList<IParameter> list;
        public ListParameter()
        {
            list = new List<IParameter>();
        }
        
        public ListParameter(params IParameter[] parameter)
        {
            list = new List<IParameter>();
            AddRange(parameter);
            
        }
        public IEnumerator<IParameter> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(IParameter item)
        {
            var hasItem = list.Any(parameter => parameter.ColumnName == item.ColumnName && parameter.TableName == item.TableName && parameter.Logical == item.Logical);
            if (!hasItem)
                list.Add(item);
        }
        
        public void AddRange(IEnumerable<IParameter> items)
        {
            foreach (var item in items)
            {
                var hasItem = list.Any(parameter => parameter.ColumnName == item.ColumnName && parameter.TableName == item.TableName && parameter.Logical == item.Logical);
                if (!hasItem)
                    list.Add(item);
            }
            
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(IParameter item)
        {
            return list.Any(parameter => parameter.ColumnName == item.ColumnName && parameter.TableName == item.TableName && parameter.Logical == item.Logical);
        }

        public void CopyTo(IParameter[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Remove(IParameter item)
        {
            var parameter = list.FirstOrDefault(x => x.ColumnName == item.ColumnName && 
                                                     x.TableName == item.TableName && 
                                                     x.Logical == item.Logical);
            return parameter != null && list.Remove(item);
        }

        public int Count => list.Count;
        public bool IsReadOnly => list.IsReadOnly;
        public int IndexOf(IParameter item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, IParameter item)
        {
            list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
        }

        public IParameter this[int index]
        {
            get => list[index];
            set => list[index] = value;
        }

    }
}