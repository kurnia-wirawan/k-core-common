using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using KCore.Common.Extensions;

namespace KCore.Common.Data
{
    public class LinqDynamicHelper
    {
        public Collection<object> ListValue { get; set; }
        
        public string GetQueryParameterLinq(params IParameter[] parameters)
        {
            var query = new StringBuilder();
            ListValue = new Collection<object>();
            var indexPass = 0;

            var total = parameters.Length - 1;
            var indexLogical = 0;
            foreach (var param in parameters)
            {
                var index = parameters.ToList().IndexOf(param) + indexPass;
                switch (param.ParamDataType)
                {
                    case EnumParameterDataTypes.DateTime:
                        var date1 = Convert.ToDateTime(param.GetValue());
                        var dateTime = date1.Date; //new DateTime(date1.Year, date1.Month, date1.Day, 0, 0, 0);
                        var dateTime2 = date1.AddDays(1).Date; //new DateTime(date1.Year, date1.Month, date1.Day, 23, 59, 59);
                        if (param.Operator == SqlOperator.Equals)
                        {
                            var param1 = WhereTerm.Default(dateTime, param.ColumnName, SqlOperator.GreatThanEqual) as IParameter;
                            var param2 = WhereTerm.Default(dateTime2, param.ColumnName, SqlOperator.LessThan) as IParameter;
                            query.Append(GetValueParameterLinq(param1, index) + DefaultValue.LOGICAL_SQL);
                            ListValue.Add(param1.GetValue());
                            index++;
                            indexPass++;
                            query.Append(GetValueParameterLinq(param2, index) + AddLogicalOperator(total, indexLogical, param));
                            ListValue.Add(param2.GetValue());
                        }
                        else
                        {
                            query.Append(
                                GetValueParameterLinq(param, index) +
                                AddLogicalOperator(total, indexLogical, param));
                            ListValue.Add(date1);
                        }
                        indexLogical++;
                    
                        break;
                    case EnumParameterDataTypes.DateTimeRange:
                    {
                        query.Append(
                            GetValueParameterLinq(param, index) +
                            AddLogicalOperator(total, indexLogical, param));
                        ListValue.Add(param.GetValue());
                        var date11 = Convert.ToDateTime(((IRangeParameter) param).GetValue2());
                        ListValue.Add(date11);
                        indexPass++;
                        indexLogical++;
                    }
                        break;
                    case EnumParameterDataTypes.Guid:
                        query.Append(
                            GetValueParameterLinq(param, index) +
                            AddLogicalOperator(total, indexLogical, param)
                            );
                        ListValue.Add(new Guid(param.GetValue().ToString()));
                        indexLogical++;
                        break;
                    case EnumParameterDataTypes.NullableDateTime:
                        query.Append(param.ColumnName + " = null " + AddLogicalOperator(total, indexLogical, param));
                        indexPass--;
                        break;
                    case EnumParameterDataTypes.Column:
                        query.Append(param.ColumnName + GetStandardOperator(param.Operator, param) +
                                     AddLogicalOperator(total, indexLogical, param));
                        indexPass--;
                        break;
                    case EnumParameterDataTypes.NumberRange:
                        query.Append(
                            GetValueParameterLinq(param, index) +
                            AddLogicalOperator(total, indexLogical, param));
                        ListValue.Add(param.GetValue());
                        var number1 = Convert.ToDouble(((IRangeParameter)param).GetValue2());
                        ListValue.Add(number1);
                        indexPass++;
                        indexLogical++;
                        break;
                    case EnumParameterDataTypes.Bool:
                        query.Append(GetValueParameterLinq(param, index) +
                            AddLogicalOperator(total, indexLogical, param));
                        ListValue.Add(param.GetValue());
                        indexLogical++;
                        break;
                    default:
                        query.Append(
                            GetValueParameterLinq(param, index) +
                            AddLogicalOperator(total, indexLogical, param));
                        ListValue.Add(param.GetValue());
                        indexLogical++;
                        break;
                }

            }
            return query.Length != 0 ? ComposeLogicalBracket(query + " AND ") : query.ToString();
        }
        
        private static string GetValueParameterLinq(IParameter param, int index)
        {
            switch (param.ParamDataType)
            {
                case EnumParameterDataTypes.Number:
                    return GetNumericLinq(param, index);
                case EnumParameterDataTypes.NumberRange:
                    return GetNumericRangeLinq(param as IRangeParameter, index);
                case EnumParameterDataTypes.DateTime:
                    return GetDateTimeLinq(param, index);
                case EnumParameterDataTypes.DateTimeRange:
                    return GetDateTimeRangeLinq(param as IRangeParameter, index);
                case EnumParameterDataTypes.Bool:
                    return GetBooleanLinq(param, index);
                case EnumParameterDataTypes.Guid:
                    return GetGuidLinq(param, index);
                default:
                    return GetCharacterLinq(param, index);
            }
        }
        
        private static string GetDateTimeRangeLinq(IRangeParameter rangeControl, int index)
        {
            return rangeControl == null ? null : $" ({rangeControl.ColumnName} >= @{index} AND {rangeControl.ColumnName2} <= @{index + 1})";
        }

        private static string GetDateTimeLinq(IParameter param, int index)
        {
            return $" ({string.Empty}{param.ColumnName} {GetOperatorDateTimeLinq(param.Operator, index)}) ";
        }

        private static string GetOperatorDateTimeLinq(SqlOperator sqlOperator, int index)
        {
            switch (sqlOperator)
            {
                case SqlOperator.NotEqual:
                    return $"<> @{index}";
                case SqlOperator.GreatThan:
                    return $"> @{index}";
                case SqlOperator.GreatThanEqual:
                    return $">= @{index}";
                case SqlOperator.LessThan:
                    return $"< @{index}";
                case SqlOperator.LesThanEqual:
                    return $"<= @{index}";
                case SqlOperator.IsNull:
                    return $"== NULL";
                default:
                    return $"= @{index}";
            }
        }

        private static string GetNumericLinq(IParameter param, int index)
        {
            return $" ({string.Empty}{param.ColumnName} {GetOperatorNumberLinq(param.Operator, index)}) ";
        }

        private static string GetNumericRangeLinq(IRangeParameter rangeControl, int index)
        {
            return rangeControl == null ? null : $" ({rangeControl.ColumnName} >= @{index} AND {rangeControl.ColumnName2} <= @{index + 1})";
        }

        private static StringBuilder GetOperatorNumberLinq(SqlOperator sqlOperator, int index)
        {
            switch (sqlOperator)
            {
                case SqlOperator.NotEqual:
                    return new StringBuilder().AppendFormat("<> @{0}", index);
                case SqlOperator.GreatThan:
                    return new StringBuilder().AppendFormat("> @{0}", index);
                case SqlOperator.GreatThanEqual:
                    return new StringBuilder().AppendFormat(">= @{0}", index);
                case SqlOperator.LessThan:
                    return new StringBuilder().AppendFormat("< @{0}", index);
                case SqlOperator.LesThanEqual:
                    return new StringBuilder().AppendFormat("<= @{0}", index);
                case SqlOperator.IsNull:
                    return new StringBuilder().AppendFormat("== NULL");
                default:
                    return new StringBuilder().AppendFormat("= @{0}", index);
            }
        }
        
        private static string GetBooleanLinq(IParameter control, int index)
        {
            return $" ({string.Empty}{control.ColumnName} {GetOperatorBooleanLinq(control.Operator, index)}) ";
        }

        private static string GetOperatorBooleanLinq(SqlOperator sqlOperator, int index)
        {
            switch (sqlOperator)
            {
                case SqlOperator.NotEqual:
                    return $"!= @{index}";
                case SqlOperator.IsNull:
                    return $"== null";
                default:
                    return $"== @{index}";
            }
        }
        
        private static string GetGuidLinq(IParameter param, int index)
        {
            return $" ({string.Empty}{param.ColumnName}{GetOperatorGuidLinq(param.Operator, index)}) ";
        }
        
        private static string GetOperatorGuidLinq(SqlOperator @operator, int param)
        {
            return @operator == SqlOperator.NotEqual ? $"<> @{param}" : $" == @{param}";
        }
        
        private static string GetCharacterLinq(IParameter param, int index)
        {
            if (param.Operator == SqlOperator.IsNull)
            {
                return "(" + (string.IsNullOrEmpty(param.TableName) ? string.Empty : param.TableName + ".") + param.ColumnName + " == null)";
            }
            return
                $" ({(string.IsNullOrEmpty(param.TableName) ? string.Empty : param.TableName + ".")}{param.ColumnName}{GetOperatorCharacterLinq(param.Operator, index)}) ";
        }

        private static string GetOperatorCharacterLinq(SqlOperator @operator, int param)
        {
            switch (@operator)
            {
                case SqlOperator.NotEqual:
                    return $".Equals(@{param}) == false";
                case SqlOperator.GreatThan:
                    return $"> @{param}";
                case SqlOperator.GreatThanEqual:
                    return $">= @{param}";
                case SqlOperator.LessThan:
                    return $"< @{param}";
                case SqlOperator.LesThanEqual:
                    return $"<= @{param}";
                case SqlOperator.BeginWith:
                    return $".StartsWith(@{param})";
                case SqlOperator.EndWith:
                    return $".EndsWith(@{param})";
                case SqlOperator.Like:
                    return $".Contains(@{param})";
                default:
                    return $".Equals(@{param})";
            }
        }
        
        private static string GetStandardOperator(SqlOperator @operator, IParameter param)
        {
            switch (@operator)
            {
                case SqlOperator.NotEqual:
                    return $"!= {param.Value}";
                case SqlOperator.GreatThan:
                    return $"> {param.Value}";
                case SqlOperator.GreatThanEqual:
                    return $">= {param.Value}";
                case SqlOperator.LessThan:
                    return $"< {param.Value}";
                case SqlOperator.LesThanEqual:
                    return $"<= {param.Value}";
                case SqlOperator.BeginWith:
                    return $".StartsWith({param.Value})";
                case SqlOperator.EndWith:
                    return $".EndsWith({param.Value})";
                case SqlOperator.Like:
                    return $".Contains({param.Value})";
                case SqlOperator.IsNull:
                    return "== null";
                default:
                    return $"== {param.Value}";
            }
        }
        
        private static string AddLogicalOperator(int total, int indexLogical, IParameter param)
        {
            return total - indexLogical == 0 ? "" : param.Logical.ToString();
        }
        
        private static string ComposeLogicalBracket(string val)
        {
            /*var val = "1 OR 2 OR 3 AND 4 OR 5 AND 6 OR 7 OR 8 OR 9 OR 10 AND 11 OR ";
            val = "1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR";
            val = "1 OR 2 AND 3 OR 4 AND 5 OR 6 AND 7 OR 8 AND 9 OR 10 AND";
            val = "1 AND 2 OR 3 AND 4 OR 5 AND 6 OR 7 AND 8 OR 9 AND 10 AND";*/
            var a = val.Split(new[] { "AND", "OR" }, StringSplitOptions.None);
            var list = a.ToList();
            if (list.Count == 1) return val;

            var result = "";
            
            var br = false;
            foreach (var item in list)
            {
                var index = list.IndexOf(item) + 1;
                if (index >= list.Count || string.IsNullOrEmpty(item.Trim()))
                    continue;
                var rr = val.Between(item.Trim(), list[index].Trim());
                if (rr.Trim().ToLower().Equals("and"))
                {
                    if (br)
                    {
                        br = false;
                        result += item + ") AND ";
                    }
                    else
                        result += item + " AND ";
                    
                }
                else
                {
                    if (!br)
                    {
                        if (index + 1 >= list.Count)
                            result += item;
                        else
                            result += "(" + item + " OR ";
                        br = true;
                        
                    }
                    else
                    {
                        if (index + 1 >= list.Count)
                        {
                            result += item + ")";
                            br = false;
                            
                        }
                        else
                        {
                            //var lg = val.Between(list[index], list[index + 1]);
                            result += item + " OR ";
                        }
                    }

                }
            }
            return result;
        }

        public string OrderBy(IEnumerable<Sort> sorts)
        {
            var result = "";

            var enumerable = sorts.ToList();
            foreach (var sort in enumerable)
            {
                var val = enumerable.IsLastIndex(sort);
                result += sort.Field + " " + sort.Direction + (!val ? ", " : "");
            }

            return result;
        }
    }
}