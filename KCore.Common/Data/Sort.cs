using System.Collections.Generic;
using Newtonsoft.Json;

namespace KCore.Common.Data
{
    public class Sort
    {
        [JsonProperty("f_")]
        public string Field { get; set; }
        [JsonProperty("d_")]
        public string Direction { get; set; }

        public static IEnumerable<Sort> Default =>
            new List<Sort>
            {
                new Sort
                {
                    Field = "ModifiedDate",
                    Direction = "DESC"
                }
            };
    }
}