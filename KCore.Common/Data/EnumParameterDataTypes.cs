namespace KCore.Common.Data
{
    public enum EnumParameterDataTypes
    {
        Character = 0,
        Number = 1,
        NumberRange = 2,
        DateTime = 3,
        DateTimeRange = 4,
        NullableDateTime = 5,
        Bool = 6,
        Guid = 7,
        RowStatus = 8,
        Column = 9
    }
}