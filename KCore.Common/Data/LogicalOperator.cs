namespace KCore.Common.Data
{
    public enum LogicalOperator
    {
        // ReSharper disable InconsistentNaming
        AND = 0,
        OR = 1
        // ReSharper restore InconsistentNaming
    }
}