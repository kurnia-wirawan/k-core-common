namespace KCore.Common.Data
{
    public static class DefaultValue
    {
        public const string COLUMN_ROW_STATUS = "RowStatus";
        public const byte ROW_STATUS_ACTIVE = 0;
        public const string LOGICAL_SQL = " AND ";
    }
}