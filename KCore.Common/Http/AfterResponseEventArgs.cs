﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace KCore.Common.Http
{
    public class AfterResponseEventArgs : EventArgs
    {
        public HttpResponseMessage Response { get; internal set; }
    }
}
