namespace KCore.Common.App
{
    public class Request
    {
        public string Method { get; set; }
        public string Url { get; set; }
        public string Body { get; set; }
        public string Client { get; set; }
    }
}