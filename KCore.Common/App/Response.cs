namespace KCore.Common.App
{
    public class Response
    {
        public string Code { get; set; }
        public string Body { get; set; }
    }
}