using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using KCore.Common.Extensions;
using KCore.Common.Fault;
using KCore.Common.Logs;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace KCore.Common.App
{
    public class GenericFailureMiddleware
    {

        private readonly RequestDelegate _next;

        public GenericFailureMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IApiLogger logger)
        {
            var currentBody = context.Response.Body;
            var errorDetail = new ErrorDetail();

            using (var memoryStream = new MemoryStream())
            {
                context.Response.Body = memoryStream;

                try
                {
                    await _next(context);
                }
                catch (ApiException e)
                {
                    logger.LogException(e);
                    context.Response.StatusCode = (int) e.StatusCode;
                    errorDetail = new ErrorDetail
                    {
                        Description = e.Message,
                        ErrorCode = e.ErrorCode.ToInteger(),
                        StatusCode = e.StatusCode
                    };
                }
                catch (Exception e)
                {
                    logger.LogException(e);
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                }
                
                context.Response.Body = currentBody;
                memoryStream.Seek(0, SeekOrigin.Begin);
                var readToEnd = new StreamReader(memoryStream).ReadToEnd();
                
                
                //Check if a pdf file should be returned
                if (string.Compare(context.Response.ContentType, "application/pdf", StringComparison.InvariantCultureIgnoreCase) == 0 || string.Compare(context.Response.ContentType, "image/png", StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    var buffer = memoryStream.ToArray();
                    await context.Response.Body.WriteAsync(buffer, 0, buffer.Length);
                    return;
                }


                if (context.Response.StatusCode == 200 && context.Response.ContentType != "application/json")
                {
                    await context.Response.WriteAsync(readToEnd);
                    return;
                }

                var objResult = JsonConvert.DeserializeObject(readToEnd);

                if (context.Response.StatusCode > 400 && objResult == null)
                {
                    var result = StatusCodeMapper.Map(context.Response.StatusCode);
                    context.Response.ContentType = "application/json; charset=utf-8";
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(result));

                }
                else
                {
                    context.Response.ContentType = "application/json; charset=utf-8";
                    if (errorDetail != null && objResult == null) objResult = errorDetail;
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(objResult));
                }

            }
        }

    }
}