using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using KCore.Common.Logs;
using Microsoft.AspNetCore.Http;

namespace KCore.Common.App
{
    public class ResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;


        public ResponseLoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IApiLogger logger)
        {
            //Tracing correlation
            if (context.Request.Headers.TryGetValue("X-Correlation-ID", out var correlationId))
            {
                context.TraceIdentifier = correlationId;
            }

            logger.LogInformation(await GetRequestLogging(context), LoggingType.Request);

            var bodyStream = context.Response.Body;
            var responseBodyStream = new MemoryStream();
            context.Response.Body = responseBodyStream;

            await _next(context);

            logger.LogInformation(await GetResponseLogging(context.Response, context.Request.Path, responseBodyStream, bodyStream), LoggingType.Response);
        }

        public async Task<Request> GetRequestLogging(HttpContext context)
        {
            var request = context.Request;
            var requestBodyStream = new MemoryStream();

            await request.Body.CopyToAsync(requestBodyStream);
            requestBodyStream.Seek(0, SeekOrigin.Begin);

            var requestBodyText = await new StreamReader(requestBodyStream).ReadToEndAsync();

            requestBodyText = Regex.Replace(requestBodyText, @"^\s*$\n", string.Empty, RegexOptions.Multiline).TrimEnd();

   
            var requestLog = new Request
            {
                Method = request.Method,
                Url = $"{request.Path}{request.QueryString}",
                Body = requestBodyText,
                Client = context.Connection.RemoteIpAddress.ToString()
            };

            requestBodyStream.Seek(0, SeekOrigin.Begin);
            request.Body = requestBodyStream;

            return requestLog;
        }

        public async Task<Response> GetResponseLogging(HttpResponse response, string path, MemoryStream responseBodyStream, Stream bodyStream)
        {
            responseBodyStream.Seek(0, SeekOrigin.Begin);
            var responseBody = new StreamReader(responseBodyStream).ReadToEnd();

            responseBody = Regex.Replace(responseBody, @"^\s*$\n", string.Empty, RegexOptions.Multiline).TrimEnd();

            var responseLog = new Response
            {
                Code = response.StatusCode.ToString(),
                Body = !IsBodyIgnoredFromResponseLogging(path) ? responseBody : "Ignored"
            };

            responseBodyStream.Seek(0, SeekOrigin.Begin);

            if (response.StatusCode != 204)
                await responseBodyStream.CopyToAsync(bodyStream);

            return responseLog;
        }

        private static bool IsBodyIgnoredFromResponseLogging(string path)
        {
            var paths = new List<string>();

            return paths.Any(x => x == path);
        }
    }
}