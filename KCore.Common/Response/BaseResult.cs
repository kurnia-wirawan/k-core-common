namespace KCore.Common.Response
{
    public class ApiResult
    {
        public bool IsSuccessful { get; }
        public bool IsFailure => !IsSuccessful;
        public string ErrorDescription { get; }

        public int HttpStatusCode { get; }
        public int ErrorCode { get; }

        internal ApiResult(bool isSuccessful, int httpStatusCode, string errorDescription, int errorCode = 0)
        {
            IsSuccessful = isSuccessful;
            ErrorDescription = errorDescription;
            HttpStatusCode = httpStatusCode;
            ErrorCode = errorCode;
        }

        public static ApiResult Ok()
        {
            return new ApiResult(true, (int)System.Net.HttpStatusCode.OK, null);
        }

        public static ApiResult Accepted()
        {
            return new ApiResult(true, (int)System.Net.HttpStatusCode.Accepted, null);
        }

        public static ApiResult Created()
        {
            return new ApiResult(true, (int)System.Net.HttpStatusCode.Created, null);
        }

        public static ApiResult NoContent()
        {
            return new ApiResult(true, (int)System.Net.HttpStatusCode.NoContent, null);
        }

        public static ApiResult Forbidden(string errorDescription)
        {
            return new ApiResult(false, (int)System.Net.HttpStatusCode.Forbidden, errorDescription);
        }

        public static ApiResult Unauthorized(string errorDescription)
        {
            return new ApiResult(false, (int)System.Net.HttpStatusCode.Unauthorized, errorDescription);
        }

        public static ApiResult BadRequest(string errorDescription)
        {
            return new ApiResult(false, (int)System.Net.HttpStatusCode.BadRequest, errorDescription);
        }

        public static ApiResult NotFound(string errorDescription, int errorCode = 0)
        {
            return new ApiResult(false, (int)System.Net.HttpStatusCode.NotFound, errorDescription, errorCode);
        }

        public static ApiResult InternalServerError(string errorDescription)
        {
            return new ApiResult(false, (int)System.Net.HttpStatusCode.InternalServerError, errorDescription);
        }

        public static ApiResult Fail(int httpStatusCode, string errordescription, int errorCode = 0)
        {
            return new ApiResult(false, httpStatusCode, errordescription, errorCode);
        }

        public static ApiResult ValidationError(string errorDescription, int errorCode = 0)
        {
            return new ApiResult(false, 460, errorDescription, errorCode);
        }

    }
}