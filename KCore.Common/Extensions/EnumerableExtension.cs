using System.Collections.Generic;
using System.Linq;

namespace KCore.Common.Extensions
{
    public static class EnumerableExtension
    {
        public static int IndexOf<T>(this IEnumerable<T> source, T value)
        {
            var index = 0;
            var comparer = EqualityComparer<T>.Default; // or pass in as a parameter
            foreach (T item in source)
            {
                if (comparer.Equals(item, value)) return index;
                index++;
            }
            return -1;
        }
        
        public static bool IsLastIndex<T>(this IEnumerable<T> source, T value)
        {
            var index = 0;
            var comparer = EqualityComparer<T>.Default; // or pass in as a parameter
            var count = source.Count();
            foreach (var item in source)
            {
                if (comparer.Equals(item, value))
                {
                    if (index + 1 == count)
                    {
                        return true;
                    }
                }
                index++;
            }
            return false;
        }
    }
}