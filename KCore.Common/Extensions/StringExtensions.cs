using System;

namespace KCore.Common.Extensions
{
    public static class StringExtensions
    {
        public static int ToInteger(this string value)
        {
            try
            {
                return Convert.ToInt32(value);
            }
            catch
            {
                return 0;
            }
        }
        
        public static short ToShort(this string value)
        {
            try
            {
                return Convert.ToInt16(value);
            }
            catch
            {
                return 0;
            }
        }
        
        public static byte ToByte(this string value)
        {
            try
            {
                return Convert.ToByte(value);
            }
            catch
            {
                return 0;
            }
        }
        
        public static string Between(this string src, string findFrom, string findTo)
        {
            var start = src.IndexOf(findFrom, StringComparison.Ordinal);
            var to = src.IndexOf(findTo, start + findFrom.Length, StringComparison.Ordinal);
            if (start < 0 || to < 0) return "";
            var s = src.Substring(
                start + findFrom.Length,
                to - start - findFrom.Length);
            return s;
        }

        public static bool Compare(this string src, string destination)
        {
            return string.Equals(src, destination, StringComparison.InvariantCultureIgnoreCase);
        }
        
        public static bool IsNullOrEmpty(this string src)
        {
            return string.IsNullOrEmpty(src);
        }
    }
}