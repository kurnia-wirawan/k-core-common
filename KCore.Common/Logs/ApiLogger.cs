using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace KCore.Common.Logs
{
    public class ApiLogger : IApiLogger
    {
        private readonly ILogger<ApiLogger> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JsonSerializerSettings _settings;

        public ApiLogger(ILogger<ApiLogger> logger, IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _settings = new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore};
        }

        public void LogInformation<T>(T message, LoggingType? loggingType = null) 
        {
            if (string.IsNullOrEmpty(_httpContextAccessor.HttpContext.Response.ContentType) || 
                (_httpContextAccessor.HttpContext.Response.ContentType != null && _httpContextAccessor.HttpContext.Response.ContentType.Contains("json")) ||
                (_httpContextAccessor.HttpContext.Response.ContentType != null && _httpContextAccessor.HttpContext.Response.ContentType.Contains("xml"))
                )
            {
                Log(new { message, loggingType = loggingType != null ? loggingType.ToString() : null, traceIdentifier = _httpContextAccessor.HttpContext.TraceIdentifier});
            }
            else
            {
                Log(new { _httpContextAccessor.HttpContext.Response.ContentType, loggingType = loggingType != null ? loggingType.ToString() : null, traceIdentifier = _httpContextAccessor.HttpContext.TraceIdentifier});    
            }
            
        }

        public void LogException(Exception ex)
        {
            _logger.LogCritical(ex.ToString());
        }

        public void LogException(string message, Exception ex)
        {
            var logging = new { message, exception = ex.ToString(), traceIdentifier = _httpContextAccessor.HttpContext.TraceIdentifier };
            _logger.LogCritical($"{JsonConvert.SerializeObject(logging, _settings)}");
        }
        
        public void LogError(string error)
        {
            _logger.LogError(new { error, traceIdentifier = _httpContextAccessor.HttpContext.TraceIdentifier }.ToString());
        }

        private void Log<T>(T contents)
        {
            var logging = $"{JsonConvert.SerializeObject(contents, _settings)}";
            _logger.LogInformation(logging);
        }

        public void LogInformation(string message)
        {
            _logger.LogInformation(message);
        }

        public void LogInformation<T>(T contents)
        {
            var logging = $"{JsonConvert.SerializeObject(contents, _settings)}";
            _logger.LogInformation(logging);
        }

        public void Log<T>(LogLevel level, EventId eventId, T state, Exception exception = null, Func<T, Exception, string> formatter = null)
        {
            _logger.Log(level, eventId, state, exception, formatter);
        }

        public void Log<T>(string message, LogLevel level, EventId eventId, T state)
        {
            _logger.Log(level, eventId, state, null, (h, e) => message );
        }
    }
}