using Microsoft.Extensions.Logging;
using System;

namespace KCore.Common.Logs
{
    public interface IApiLogger
    {
        void LogInformation<T>(T message, LoggingType? loggingType = null);
        void LogException(Exception ex);
        void LogException(string message, Exception ex);
        void LogError(string error);
        void LogInformation(string message);
        void LogInformation<T>(T contents);
        void Log<T>(LogLevel level, EventId eventId, T state, Exception exception = null, Func<T, Exception, string> formatter = null);
        void Log<T>(string message, LogLevel level, EventId eventId, T state);
    }
    
    public enum LoggingType
    {
        Unknown = 0,
        Request = 1,
        Response = 2,
        Authentication = 3
    }
}