using System;
using System.Net;

namespace KCore.Common.Fault
{
    public class ApiException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        public ApiException(string message, HttpStatusCode status, string resultErrorCode) : base(message)
        {
            StatusCode = status;
            ErrorCode = resultErrorCode;
        }

        public string ErrorCode { get; set; }

        public ApiException(string message, HttpStatusCode status, Exception innerException) : base(message, innerException)
        {
            StatusCode = status;
        }
        
        public ApiException(ErrorDetail error) : base(error.Description)
        {
            StatusCode = error.StatusCode;
            ErrorCode = error.ErrorCode.ToString();
        }
    }
}