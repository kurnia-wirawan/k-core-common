namespace KCore.Common.Fault
{
    public class ValidationResult
    {
        public bool IsSuccessful { get; }
        public bool IsFailure => !IsSuccessful;
        public string ErrorDescription { get; set; }
        public string ErrorCode { get; set; }

        public int HttpStatusCode { get; }
        public string Error;

        internal ValidationResult(bool isSuccessful, int httpStatusCode, string errorDescription, string errorCode)
        {
            IsSuccessful = isSuccessful;
            ErrorCode = errorCode;
            ErrorDescription = errorDescription;
            HttpStatusCode = httpStatusCode;
        }

        public static ValidationResult Ok()
        {
            return new ValidationResult(true, (int)System.Net.HttpStatusCode.OK, null, null);
        }

        public static ValidationResult Forbidden(string errorDescription, string errorCode = null)
        {
            return new ValidationResult(false, (int)System.Net.HttpStatusCode.Forbidden, errorDescription, errorCode);
        }

        public static ValidationResult Unauthorized(string errorDescription, string errorCode = null)
        {
            return new ValidationResult(false, (int)System.Net.HttpStatusCode.Unauthorized, errorDescription, errorCode);
        }

        public static ValidationResult BadRequest(string errorDescription, string errorCode = null)
        {
            return new ValidationResult(false, (int)System.Net.HttpStatusCode.BadRequest, errorDescription, errorCode);
        }

        public static ValidationResult NotFound(string errorDescription, string errorCode = null)
        {
            return new ValidationResult(false, (int)System.Net.HttpStatusCode.NotFound, errorDescription, errorCode);
        }

        public static ValidationResult InternalServerError(string errorDescription, string errorCode = null)
        {
            return new ValidationResult(false, (int)System.Net.HttpStatusCode.InternalServerError, errorDescription, errorCode);
        }

        public static ValidationResult NotAcceptable(string errorDescription, string errorCode = null)
        {
            return new ValidationResult(false, (int)System.Net.HttpStatusCode.NotAcceptable, errorDescription, errorCode);
        }

        public static ValidationResult ValidationError(string errorDescription, string errorCode = null)
        {
            return new ValidationResult(false, 460, errorDescription, errorCode);
        }
        
        public static ValidationResult ValidationError(ErrorDetail errorCode)
        {
            return new ValidationResult(false, 460, errorCode.Description, errorCode.ErrorCode.ToString());
        }

        public static ValidationResult ErrorDuringProcessing(string errorDescription)
        {
            // Something went wrong. Please try again in a few minutes or contact your support team.
            // Terjadi kesalahan pada saat proses di Web Server. Silahkan hubungi tim support kami.
            return new ValidationResult(false, 461, string.IsNullOrEmpty(errorDescription) ? 
                "Something went wrong. Please try again in a few minutes or contact your support team." : errorDescription, null);
        }

    }
}