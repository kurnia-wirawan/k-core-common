using System.Threading.Tasks;

namespace KCore.Common.Fault
{
    public interface IRequestValidator<in TRequest>
    {
        Task<ValidationResult> InternalValidate(TRequest request);
        int Order { get; }
    }
}