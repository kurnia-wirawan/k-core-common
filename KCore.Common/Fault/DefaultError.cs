namespace KCore.Common.Fault
{
    public class DefaultError
    {
        public static ErrorDetail InvalidApplicationRegistration => new ErrorDetail{ Description = "Kesalahan, Aplikasi Key sudah tidak aktif", ErrorCode = 600};
        public static ErrorDetail InvalidApplicationConfiguration => new ErrorDetail{ Description = "Invalid application config", ErrorCode = 604};
        public static ErrorDetail InvalidRequest => new ErrorDetail{ Description = "Kesalahan, Request NULL", ErrorCode = 601};
        public static ErrorDetail InvalidUser => new ErrorDetail{ Description = "Kesalahan, User tidak terdaftar", ErrorCode = 602};
        public static ErrorDetail InvalidCompany => new ErrorDetail { Description = "Kesalahan, Company tidak terdaftar", ErrorCode = 603 };
        public static ErrorDetail DataNotFound => new ErrorDetail { Description = "Kesalahan, Data Tidak Ditemukan", ErrorCode = 605 };
        public static ErrorDetail InvalidJArray=> new ErrorDetail { Description = "Error, invalid json array data", ErrorCode = 701 };

    }
}