using System.Collections.Generic;
using System.Net;

namespace KCore.Common.Fault
{
    public class StatusCodeMapper
    {
        private static readonly Dictionary<int, ErrorDetail> StatusCodeMap =
            new Dictionary<int, ErrorDetail>
            {
                {
                    (int) HttpStatusCode.Unauthorized,
                    new ErrorDetail
                        {ErrorCode = 900, Description = "Anda tidak memiliki hak akses"}
                },
                {
                    (int) HttpStatusCode.NotFound,
                    new ErrorDetail {ErrorCode = 910, Description = "Sumber tidak terdaftar"}
                },
                {
                    (int) HttpStatusCode.InternalServerError,
                    new ErrorDetail
                    {
                        ErrorCode = 920,
                        Description =
                            "Terjadi kesalahan. Silahkan coba kembali beberapa menit atau hubungi tim support kami."
                    }
                },
                {
                    461,
                    new ErrorDetail
                    {
                        ErrorCode = 9999,
                        Description =
                            "Terjadi kesalahan pada saat proses request. Silahkan hubungi tim support kami."
                    }
                }
            };

        public static ErrorDetail Map(int statusCode)
        {
            return StatusCodeMap.GetValueOrDefault(statusCode, null);
        }
    }
}