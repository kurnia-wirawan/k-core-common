using System.Collections.Generic;
using System.Linq;
using Fluency;
using Omu.ValueInjecter;

namespace KCore.Common.Test
{
    public class BaseFluentBuilder<T> : FluentBuilder<T> where T : class
    {
        protected const byte RowStatus = 0;

        public static IEnumerable<T> Convert<T2>(IEnumerable<T2> members) where T2: class
        {
            return members.Select(m => Mapper.Map<T2, T>(m)).ToList();
        }
        
        
    }
}